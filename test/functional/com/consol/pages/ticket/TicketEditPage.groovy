package com.consol.pages.ticket

import geb.Module
import geb.Page

class TicketEditPage extends Page {

    static url = "ticket#/create"

    static at = { $('h2').text() == 'Edit Ticket' }

    static content = {
		subjectField {$("input[ng-model='ctrl.ticket.subject']")}
        saveButton { $('button[crud-button="save"]') }
    }

}