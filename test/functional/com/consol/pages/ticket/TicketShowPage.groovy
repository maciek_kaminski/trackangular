package com.consol.pages.ticket

import geb.Page

class TicketShowPage extends Page {

    static at = { $('h2').text().startsWith 'Show Ticket' }

    static content = {
        successMessage { $(".alert-success") }
    }

}