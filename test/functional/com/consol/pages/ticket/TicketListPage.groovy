package com.consol.pages.ticket

import geb.Module
import geb.Page

class TicketListPage extends Page {

    static url = "ticket"

    static at = { $('h2').text() == 'Ticket List' }

    static content = {
		subjectFilter {$("input[ng-model='ctrl.filter.subject']")}
	
		subjectSort { $("table#list th[property='subject']") }
    
	    createButton { $("button[crud-button='create']") }
        successMessage { $(".alert-success") }
		
        rows { moduleList TicketListRow, $("table#list tbody tr") }
    }

}

class TicketListRow extends Module {

	static content = {
		cell { $("td") }
        editButton {$("button[crud-button='edit']")}
        deleteButton {$("button[crud-button='delete']")}
    }

}