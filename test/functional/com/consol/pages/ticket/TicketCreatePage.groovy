package com.consol.pages.ticket

import geb.Module
import geb.Page

class TicketCreatePage extends Page {

    static url = "ticket#/create"

    static at = { $('h2').text() == 'Create Ticket' }

    static content = { 
		subjectField {$("input[ng-model='ctrl.ticket.subject']")}
        saveButton { $('button[crud-button="save"]') }
    }

}