package com.consol

import com.consol.pages.ticket.*
import geb.spock.GebReportingSpec


class TicketFunctionalSpec extends GebReportingSpec {

	def "should be able to view list page"() {
		when:
		to TicketListPage

		then:
		at TicketListPage
	}
	
	def "should be able to create a valid Ticket"() {
		when:
		to TicketListPage

		and:
		createButton.click()

		then:
		at TicketCreatePage

		when:
		subjectField = "Foo"
			
		and:
		saveButton.click()

		then:
		at TicketShowPage

		and:
		successMessage.displayed

		and:
		successMessage.text().contains "Ticket was successfully created"
	}
	
	def "should be able to sort the Ticket List"() {
		given:
		to TicketListPage

		when:
		subjectSort.click()
		
		then:
		subjectSort.classes().contains("asc")
	
	}
	
	def "should be able to filter the Ticket List"() {
		given:
		to TicketListPage

		when:
		subjectFilter = "Foo"
		
		then:
		waitFor { rows.size() > 0 }
	
	}
	
	def "should be able to edit the first Ticket"() {
		when:
		to TicketListPage

		and:
		rows.first().editButton.click()

		then:
		at TicketEditPage
		
		when:
		subjectField = "Foo!"
		
		and:
		saveButton.click()
		
		then:
		at TicketShowPage

		and:
		successMessage.displayed

		and:
		successMessage.text().contains "Ticket was successfully updated"
	}
	
	def "should be able to delete the first Ticket"() {
		when:
		to TicketListPage

		and:
		rows.first().deleteButton.click()

		then:
		at TicketListPage

		and:
		successMessage.displayed

		and:
		successMessage.text().contains "Ticket was successfully deleted"
      }
	
}