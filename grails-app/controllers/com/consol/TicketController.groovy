package com.consol
import com.consol.Ticket
import grails.gorm.PagedResultList
import groovyx.net.http.HttpResponseException
import groovyx.net.http.RESTClient
import org.apache.http.client.HttpResponseException

class TicketController extends PagedRestfulController {
    TicketController() {
        super(Ticket)
    }

    def index(Integer max) {
        params.page = params.int('page') ?: 0
        params.max = params.int('pageSize') ?: grailsApplication.config.angular.pageSize ?: 10
        params.offset = ((params.page - 1) * params.max)

        RESTClient restClient = new RESTClient("http://Huber:consol@localhost:8888/restapi/")
        restClient.headers['X-Template'] = 'rest_response_ticketextlist';
        try {
            def groupResponse = restClient.get([
                    path:'tickets',
                    contentType:'application/json',
                    query : [pageSize:params.max, pageNumber: params.page-1]])

            def json = groupResponse.data;
            for (Object ticket : json.tickets.ticket){

                ticket['class']="com.consol.Ticket";
                ticket['name']=ticket['@name'];
                ticket['id']=ticket['@id'];

                ticket.remove('@id');
                ticket.remove('@name');
            }

            response.setHeader('Content-Range', getContentRange(Integer.valueOf(json.totalNumberOfElements), params.offset, params.max))
            respond json.tickets.ticket, formats: ['json', 'html']
        } catch (HttpResponseException ex) {
            // errors and response status codes >= 400 end up here
        }
    }


    def show() {
        RESTClient restClient = new RESTClient("http://Huber:consol@localhost:8888/restapi/")
        restClient.headers['X-Template'] = 'rest_response_ticket_v2';
        try {
            def groupResponse = restClient.get([
                    path:'tickets/'+params.id,
                    contentType:'application/json'])

            def ticket = groupResponse.data;

            ticket['class']="com.consol.Ticket";
            ticket['name']=ticket['@name'];
            ticket['id']=ticket['@id'];
            //TODO
            ticket['queue']='Helpdesk';

            ticket.remove('@id');
            ticket.remove('@name');
            respond ticket, formats: ['json', 'html']
        } catch (HttpResponseException ex) {
            // errors and response status codes >= 400 end up here
        }
    }

}