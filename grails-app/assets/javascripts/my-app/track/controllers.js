'use strict';

function ListCtrl($scope, TicketResource, ticketList, pageSize, dateFormat) {
    var self = this;

    this.todayDate = new Date();

    this.refreshTicket = function(index, id){
        TicketResource.get(id).then(function(item) {
            item.name = item.name.toUpperCase();
            self.ticketList[index] = item;
        });
    }
    self.ticketList = ticketList;


    self.setPageSize = function(size){
        self.pageSize = size;
        self.load();
    }

    self.pageSize = pageSize;
    self.pageSizeList = [10,20,50];
    self.dateFormat = dateFormat;
    self.page = 1;
    self.filter = {};

    $scope.$watchCollection(function() { return self.filter }, function(newValue, oldValue) {
        self.reload();
    });

    $scope.$watch(function() { return self.page}, function(newValue, oldValue) {
        if(newValue !== oldValue){
            self.load();
        }
    });

    self.load = function() {
        var params = {page: self.page, pageSize: self.pageSize};

        if (self.sort) {
            angular.extend(params, self.sort);
        }
		if (self.filter) {
			params.filter = self.filter
		}

        TicketResource.list(params).then(function(items) {
            self.ticketList = items;
            self.totalTicketsNumber = items.getTotalCount();
        });
    };

    self.reload = function() {
        self.page = 1;
        self.pageSize = pageSize;
        self.load();
    }
}

function NavCtrl($scope, $location) {
    $scope.menuClass = function() {
        var current = $location.path();
        for (var i = 0; i < arguments.length; i++) {
            if(arguments[i] === current) {
                return "active";
            }
        }
        return "";
    };
}


function ShowCtrl(ticket, dateFormat) {
    var self = this;
    self.dateFormat = dateFormat;
    self.ticket = ticket;
};

function CreateEditCtrl(ticket ) {
    var self = this;
	
    self.ticket = ticket;
}

angular.module('myApp.track.controllers', ['ui.bootstrap'])
    .controller('NavCtrl', NavCtrl)
    .controller('ListCtrl', ListCtrl)
    .controller('ShowCtrl', ShowCtrl)
    .controller('CreateEditCtrl', CreateEditCtrl);