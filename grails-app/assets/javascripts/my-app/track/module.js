//= require_self 
//= require controllers
//= require services 
//= require_tree /my-app/track/templates/

'use strict';

var APP = angular.module('myApp.track', [
	'grails', 
	'myApp.track.controllers',
	'myApp.track.services'
])
.value('defaultCrudResource', 'TicketResource')
.config(function($routeProvider) {
	$routeProvider
        .when('/', {
            controller: 'ListCtrl as ctrl',
            templateUrl: 'list.html',
            resolve: {
                ticketList: function($route, TicketResource) {
                    var params = $route.current.params;
                    //return TicketResource.list(params); // using this woul load all tickets without pagination!
                    return [];
                } 
            }
        })
        .when('/create', {
            controller: 'CreateEditCtrl as ctrl',
            templateUrl: 'create-edit.html',
            resolve: {
                ticket: function(TicketResource) {
                    return TicketResource.create();
                } 
            }
        })
        .when(';', {
            controller: 'CreateEditCtrl as ctrl',
            templateUrl: 'create-edit.html',
            resolve: {
                ticket: function($route, TicketResource) {
                    var id = $route.current.params.id;
                    return TicketResource.get(id);
                } 
            }
        })
        .when('/show/:id', {
            controller: 'ShowCtrl as ctrl',
            templateUrl: 'show.html',
            resolve: {
                ticket: function($route, TicketResource) {
                    var id = $route.current.params.id;
                    return TicketResource.get(id);
                }
            }
        })
        .otherwise({redirectTo: '/'});
});



APP.directive('ngNavi', function() {
    return {
        restrict: 'E',
        templateUrl: 'nav.html',
        controller: 'NavCtrl'
    }
}).directive('ngFooter', function() {
    return {
        restrict: 'E',
        templateUrl: 'footer.html',
    }
});
