'use strict';

function TicketResource(CrudResourceFactory) {
    return CrudResourceFactory('/api/ticket', 'Ticket');
}

angular.module('myApp.track.services', ['grails'])
    .factory('TicketResource', TicketResource);
