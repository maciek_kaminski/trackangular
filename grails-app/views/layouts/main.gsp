<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>

<body id="ng-app" ng-app="${pageProperty(name: 'body.ng-app') ?: 'grails'}" class="body-navbar-fixed-top">

    <ng-navi></ng-navi>

    <div class="animate-view" ng-view></div>
        <g:layoutBody/>
    </div>

    <ng-footer></ng-footer>

    <asset:script type="text/javascript">
        angular.module('grails.constants')
            .constant('rootUrl', '${request.contextPath}')
            .constant('pageSize', ${grailsApplication.config.angular.pageSize})
            .constant('dateFormat', '${grailsApplication.config.angular.dateFormat}');
    </asset:script>

    <asset:javascript src="application.js"/>
    <asset:deferredScripts/>
    <g:pageProperty name="page.scripts" default=""/>
</body>
</html>
