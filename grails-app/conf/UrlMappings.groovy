class UrlMappings {

	static mappings = {
		'/api/ticket'(resources: 'ticket')
        "/"(view:"index")

        "500"(view:'/error')
        "/$controller/$action?/$id?(.$format)?"{
                    constraints {
                        // apply constraints here
                    }
                }
	}
}
