import com.consol.Engineer
import com.consol.Queue
import com.consol.Ticket
import grails.converters.JSON

class BootStrap {

	def customMarshallerRegistrar
    def grailsApplication

    def init = { servletContext ->
		customMarshallerRegistrar.registerMarshallers()

        def filePath = "resources/ticketData.json"
        def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
        def json = JSON.parse(text)

        for (ticketData in json){
            def e = new Engineer(firstname: ticketData["engineerFirstname"],lastname: ticketData["engineerLastname"]).save(failOnError: true);
            def q = new Queue(name: ticketData["queue"]).save(failOnError: true);
            def c = new Ticket(
                    subject: ticketData ["subject"],
                    name: ticketData ["name"],
                    engineer: e,
                    queue: q
            ).save(failOnError: true);
        }

	}
    def destroy = {
    }
}
