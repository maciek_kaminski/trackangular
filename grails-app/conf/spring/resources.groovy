// Place your Spring DSL code here
import com.consol.CustomMarshallerRegistrar

beans = {
    customMarshallerRegistrar(CustomMarshallerRegistrar)
}
