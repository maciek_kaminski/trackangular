package com.consol

class Engineer {

    String firstname;
    String lastname;

    static constraints = {
    }

    def name() {
        return firstname + " " + lastname;
    }
}
